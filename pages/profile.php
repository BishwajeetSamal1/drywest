<!DOCTYPE html>
<!--
Template Name: Drywest
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html lang="">
  <!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
  <head>
    <title>Login
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
    <!-- <link rel="stylesheet" href="../layout/styles/style.css" > -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  </head>
  <body id="top">
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row0">
      <div id="topbar" class="hoc clear"> 
        <!-- ################################################################################################ -->
        <ul>
          <li>
            <i class="fa fa-clock-o">
            </i> Mon. - Fri. 8am - 5pm
          </li>
          <li>
            <i class="fa fa-phone">
            </i> +00 (123) 456 7890
          </li>
          <li>
            <i class="fa fa-envelope-o">
            </i> info@domain.com
          </li>
          <li>
            <a href="#">
              <i class="fa fa-lg fa-home">
              </i>
            </a>
          </li>
          <li>
            <a href="#" title="Login">
              <i class="fa fa-lg fa-sign-in">
              </i>
            </a>
          </li>
          <li>
            <a href="#" title="Sign Up">
              <i class="fa fa-lg fa-edit">
              </i>
            </a>
          </li>
        </ul>
        <!-- ################################################################################################ -->
      </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row1">
      <header id="header" class="hoc clear"> 
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
          <h1>
            <a href="../index.html">OLEICHANDANPUR SCHOOL
            </a>
          </h1>
        </div>
        <div class="fl_right">
          <a class="btn" href="#">Quam quisque vel
          </a>
        </div>
        <!-- ################################################################################################ -->
      </header>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- Top Background Image Wrapper -->
    <div class="bgded" style="background-image:url('../images/demo/backgrounds/01.png');"> 
      <!-- ################################################################################################ -->
      <div class="wrapper row2">
        <nav id="mainav" class="hoc clear"> 
          <!-- ################################################################################################ -->
          <ul class="clear">
            <li class="active">
              <a href="index.html">Home
              </a>
            </li>
            <!-- <li><a class="drop" href="#">Pages</a>
<ul>
<li><a href="pages/gallery.html">About</a></li>
<li><a href="pages/full-width.html">Admission</a></li>
<li><a href="pages/sidebar-left.html">Academics</a>
<ul>
<li><a href="">dddd</a></li>
</ul>
</li>
<li><a href="pages/sidebar-right.html">Sidebar Right</a></li>
<li><a href="pages/basic-grid.html">Basic Grid</a></li>
</ul>
</li> -->
            <li>
              <a  href="#">About
              </a>
              <ul>
                <li>
                  <a href="#">Leadership
                  </a>
                </li>
                <li>
                  <a href="#">Our Mission
                  </a>
                </li>
                <li>
                  <a href="#">Our Vision
                  </a>
                </li>
                <li>
                  <a href="#">Our Identity
                  </a>
                </li>
                <li>
                  <a href="#">Campus
                  </a>
                </li>
                <li>
                  <a href="#">History
                  </a>
                </li>
                <li>
                  <a href="#">Publication
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Admission
              </a>
              <ul>
                <li>
                  <a href="#">Apply Now
                  </a>
                </li>
                <li>
                  <a href="#">Fees Structure
                  </a>
                </li>
                <li>
                  <a href="#">Virtual Tour
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Academics
              </a>
              <ul>
                <li>
                  <a href="#">Curriculum
                  </a>
                </li>
                <li>
                  <a href="#">IB Diploma Programme
                  </a>
                </li>
                <li>
                  <a href="#">Middle Years
                  </a>
                </li>
                <li>
                  <a href="#">Early Years
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Student Life
              </a>
              <ul>
                <li>
                  <a href="#">Resident Life
                  </a>
                </li>
                <li>
                  <a href="#">Attendance Report
                  </a>
                </li>
                <li>
                  <a href="#">Personal Counselling
                  </a>
                </li>
                <li>
                  <a href="#">Student Services
                  </a>
                </li>
                <li>
                  <a href="#">Religious Life
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Enrichmment
              </a>
              <ul>
                <li>
                  <a href="#">Outdoor Learning
                  </a>
                </li>
                <li>
                  <a href="#">Centre for Imagination
                  </a>
                </li>
                <li>
                  <a href="#">Sports
                  </a>
                </li>
                <li>
                  <a href="#">Community
                  </a>
                </li>
                <li>
                  <a href="#">Engagement
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">Giving
              </a>
            </li>
            <li>
              <a href="#">Summer
              </a>
            </li>
            <li>
              <a href="#">Login
              </a>
              <ul>
                <li>
                  <a href="signup.php">Parents
                  </a>
                </li>
                <li>
                  <a href="signup.php">Staaff & Student
                  </a>
                </li>
              </ul>
            </li>
            <!-- ################################################################################################ -->
            </nav>
          </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper overlay">
          <div id="breadcrumb" class="hoc clear"> 
            <!-- ################################################################################################ -->
            <ul>
              <li>
                <a href="#">Home
                </a>
              </li>
              <li>
                <a href="#">Lorem
                </a>
              </li>
              <li>
                <a href="#">Ipsum
                </a>
              </li>
              <li>
                <a href="#">Dolor
                </a>
              </li>
            </ul>
            <!-- ################################################################################################ -->
          </div>
        </div>
        <!-- ################################################################################################ -->
      </div>
      <!-- End Top Background Image Wrapper -->
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <div class="wrapper row3">
        <main class="hoc container clear"> 
          <!-- main body -->
          <!-- ################################################################################################ -->
          <!-- ################################################################################################ -->
          <!-- / main body -->
          <div class="clear">
          </div>
          <div class="container">
            <div class="section-header">
              <h2>Profile
              </h2>
              <div class="card-columns">
                <div class="card bg-primary">
                  <div class="card-body text-center">
                   <h5><a href="#" class="color">Profile Information</a></h5>
                  </div>
                </div>
                <div class="card bg-warning">
                  <div class="card-body text-center">
                  <h5><a href="#" class="color">Attendance</a></h5>
                  </div>
                </div>
                <div class="card bg-success">
                  <div class="card-body text-center">
                  <h5><a href="#" class="color">Monday Test Report</a></h5>
                  </div>
                </div>
              </div>
              <div class="card-columns">
                <div class="card bg-success">
                  <div class="card-body text-center">
                  <h5><a href="#" class="color">Monthly Test Report</a></h5>
                  </div>
                </div>
                <div class="card bg-warning">
                  <div class="card-body text-center">
                  <h5><a href="#" class="color">Rank</a></h5>
                  </div>
                </div>
                <div class="card bg-primary">
                  <div class="card-body text-center">
                  <h5><a href="#" class="color">Health Report</a></h5>
                  </div>
                </div>
                <!--######################################################-->
                <!-- <div class="row nobita_space">
<div class="column">
<div class="card">
<h3>Card 1</h3>
<p>Some text</p>
<p>Some text</p>
</div>
</div>
<div class="column">
<div class="card">
<h3>Card 2</h3>
<p>Some text</p>
<p>Some text</p>
</div>
</div>
<div class="column">
<div class="card">
<h3>Card 3</h3>
<p>Some text</p>
<p>Some text</p>
</div>
</div>
</div> -->
              </div>
            </div>
            </main>
          </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row4">
          <footer id="footer" class="hoc clear"> 
            <!-- ################################################################################################ -->
            <div class="one_third first">
              <h6 class="heading">Arcu accumsan id felis
              </h6>
              <ul class="nospace btmspace-30 linklist contact">
                <li>
                  <i class="fa fa-map-marker">
                  </i>
                  <address>
                    Street Name &amp; Number, Town, Postcode/Zip
                  </address>
                </li>
                <li>
                  <i class="fa fa-phone">
                  </i> +00 (123) 456 7890
                </li>
                <li>
                  <i class="fa fa-envelope-o">
                  </i> info@domain.com
                </li>
              </ul>
              <ul class="faico clear">
                <li>
                  <a class="faicon-facebook" href="#">
                    <i class="fa fa-facebook">
                    </i>
                  </a>
                </li>
                <li>
                  <a class="faicon-twitter" href="#">
                    <i class="fa fa-twitter">
                    </i>
                  </a>
                </li>
                <li>
                  <a class="faicon-dribble" href="#">
                    <i class="fa fa-dribbble">
                    </i>
                  </a>
                </li>
                <li>
                  <a class="faicon-linkedin" href="#">
                    <i class="fa fa-linkedin">
                    </i>
                  </a>
                </li>
                <li>
                  <a class="faicon-google-plus" href="#">
                    <i class="fa fa-google-plus">
                    </i>
                  </a>
                </li>
                <li>
                  <a class="faicon-vk" href="#">
                    <i class="fa fa-vk">
                    </i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="one_third">
              <h6 class="heading">Sodales facilisis nisi
              </h6>
              <ul class="nospace linklist">
                <li>
                  <article>
                    <h2 class="nospace font-x1">
                      <a href="#">Curabitur a pharetra
                      </a>
                    </h2>
                    <time class="font-xs block btmspace-10" datetime="2045-04-06">Friday, 6
                      <sup>th
                      </sup> April 2045
                    </time>
                    <p class="nospace">Quis quam in hendrerit elit donec hendrerit sollicitudin imperdiet curabitur [&hellip;]
                    </p>
                  </article>
                </li>
                <li>
                  <article>
                    <h2 class="nospace font-x1">
                      <a href="#">Risus sit amet blandit
                      </a>
                    </h2>
                    <time class="font-xs block btmspace-10" datetime="2045-04-05">Thursday, 5
                      <sup>th
                      </sup> April 2045
                    </time>
                    <p class="nospace">Ultricies aliquam nisi vitae pulvinar donec luctus ex ex eget ornare tortor [&hellip;]
                    </p>
                  </article>
                </li>
              </ul>
            </div>
            <div class="one_third">
              <h6 class="heading">Justo duis vulputate
              </h6>
              <p class="nospace btmspace-30">commodo non proin fermentum neque sapien phasellus molestie tincidunt.
              </p>
              <form method="post" action="#">
                <fieldset>
                  <legend>Newsletter:
                  </legend>
                  <input class="btmspace-15" type="text" value="" placeholder="Name">
                  <input class="btmspace-15" type="text" value="" placeholder="Email">
                  <button type="submit" value="submit">Submit
                  </button>
                </fieldset>
              </form>
            </div>
            <!-- ################################################################################################ -->
          </footer>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row5">
          <div id="copyright" class="hoc clear"> 
            <!-- ################################################################################################ -->
            <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - 
              <a href="#">Domain Name
              </a>
            </p>
            <p class="fl_right">Template by 
              <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates
              </a>
            </p>
            <!-- ################################################################################################ -->
          </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <a id="backtotop" href="#top">
          <i class="fa fa-chevron-up">
          </i>
        </a>
        <!-- JAVASCRIPTS -->
        <script src="../layout/scripts/jquery.min.js">
        </script>
        <script src="../layout/scripts/jquery.backtotop.js">
        </script>
        <script src="../layout/scripts/jquery.mobilemenu.js">
        </script>
        </body>
      </html>
