<!DOCTYPE html>
<!--
Template Name: Drywest
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html lang="">
<!-- To declare your language - read more here: https://www.w3.org/International/questions/qa-html-language-declarations -->
<head>
<title>Login</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<!-- <link rel="stylesheet" href="../layout/styles/style.css" > -->
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><i class="fa fa-clock-o"></i> Mon. - Fri. 8am - 5pm</li>
      <li><i class="fa fa-phone"></i> +00 (123) 456 7890</li>
      <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      <li><a href="#"><i class="fa fa-lg fa-home"></i></a></li>
      <li><a href="#" title="Login"><i class="fa fa-lg fa-sign-in"></i></a></li>
      <li><a href="#" title="Sign Up"><i class="fa fa-lg fa-edit"></i></a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
      <h1><a href="../index.html">OLEICHANDANPUR SCHOOL</a></h1>
    </div>
    <div class="fl_right"><a class="btn" href="#">Quam quisque vel</a></div>
    <!-- ################################################################################################ -->
  </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->
<div class="bgded" style="background-image:url('../images/demo/backgrounds/01.png');"> 
  <!-- ################################################################################################ -->
  <div class="wrapper row2">
    <nav id="mainav" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <ul class="clear">
        <li class="active"><a href="index.html">Home</a></li>
        <!-- <li><a class="drop" href="#">Pages</a>
          <ul>
            <li><a href="pages/gallery.html">About</a></li>
            <li><a href="pages/full-width.html">Admission</a></li>
            <li><a href="pages/sidebar-left.html">Academics</a>
            <ul>
            <li><a href="">dddd</a></li>
            </ul>
            </li>
            <li><a href="pages/sidebar-right.html">Sidebar Right</a></li>
            <li><a href="pages/basic-grid.html">Basic Grid</a></li>
          </ul>
        </li> -->
        <li><a  href="#">About</a>
          <ul>
            <li><a href="#">Leadership</a></li>
            <li><a href="#">Our Mission</a></li>
            <li><a href="#">Our Vision</a></li>
            <li><a href="#">Our Identity</a></li>
            <li><a href="#">Campus</a></li>
            <li><a href="#">History</a></li>
            <li><a href="#">Publication</a></li>
          </ul>
        </li>
        <li><a href="#">Admission</a>
        <ul>
        <li><a href="#">Apply Now</a></li>
        <li><a href="#">Fees Structure</a></li>
        <li><a href="#">Virtual Tour</a></li>
        </ul></li>
        <li><a href="#">Academics</a>
        <ul>
        <li><a href="#">Curriculum</a></li>
        <li><a href="#">IB Diploma Programme</a></li>
        <li><a href="#">Middle Years</a></li>
        <li><a href="#">Early Years</a></li>
        </ul>
        </li>
        <li><a href="#">Student Life</a>
        <ul>
        <li><a href="#">Resident Life</a></li>
        <li><a href="#">Attendance Report</a></li>
        <li><a href="#">Personal Counselling</a></li>
        <li><a href="#">Student Services</a></li>
        <li><a href="#">Religious Life</a></li>
        </ul>
        </li>
        <li><a href="#">Enrichmment</a>
        <ul>
        <li><a href="#">Outdoor Learning</a></li>
        <li><a href="#">Centre for Imagination</a></li>
        <li><a href="#">Sports</a></li>
        <li><a href="#">Community</a></li>
        <li><a href="#">Engagement</a></li>
        </ul>
        </li>
        <li><a href="#">Giving</a></li>
        <li><a href="#">Summer</a></li>
        <li><a href="#">Login</a>
        <ul>
        <li><a href="signup.php">Parents</a></li>
        <li><a href="signup.php">Staff & Student</a></li>
        </ul>
        </li>
      <!-- ################################################################################################ -->
    </nav>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper overlay">
    <div id="breadcrumb" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Lorem</a></li>
        <li><a href="#">Ipsum</a></li>
        <li><a href="#">Dolor</a></li>
      </ul>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- ################################################################################################ -->
</div>
<!-- End Top Background Image Wrapper -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
    <section id="contact" class="wow fadeInUp">
                <div class="container">
                    <div class="section-header">
                        <h2>Login Form</h2>
                         </div>
                </div>
                <!-- <div class="container">
                    <div class="form">
                        <form method="post" role="form" class="contactForm" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" name="fname" class="form-control" id="fname" placeholder="First Name" data-rule="minlen:1" data-msg="Please Enter First Name" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" name="lname" class="form-control" id="lname" placeholder="Last Name" data-rule="minlen:1" data-msg="Please Enter last Name" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please Enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" name="address" class="form-control" id="address" placeholder="Address" data-rule="minlen:1" data-msg="Please Enter Address" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" name="city_residence" class="form-control" id="city_residence" placeholder="City Residence" data-rule="minlen:1" data-msg="Please Enter city residence" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" name="country_residence" class="form-control" id="country_residence" placeholder="Country Residence" data-rule="minlen:1" data-msg="Please Enter country residence" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <select id="from" class="form-control">
                                        <option value="">Source Language</option>
                                        <option value="English">English</option>
                                        <option value="Afrikaans">Afrikaans</option>
                                        <option value="Albanian">Albanian</option>
                                        <option value="Amharic">Amharic</option>
                                        <option value="Arabic">Arabic</option>
                                        <option value="Armenian">Armenian</option>
                                        <option value="Azeri">Azeri</option>
                                        <option value="Bantu">Bantu</option>
                                        <option value="Belarusan">Belarusan</option>
                                        <option value="Bengali">Bengali</option>
                                        <option value="Bosnian">Bosnian</option>
                                        <option value="Bulgarian">Bulgarian</option>
                                        <option value="Burmese">Burmese</option>
                                        <option value="Cambodian">Cambodian</option>
                                        <option value="Catalan">Catalan</option>
                                        <option value="Chinese">Chinese</option>
                                        <option value="Creole">Creole</option>
                                        <option value="Croatian">Croatian</option>
                                        <option value="Czech">Czech</option>
                                        <option value="Danish">Danish</option>
                                        <option value="Dari">Dari</option>
                                        <option value="Dutch">Dutch</option>
                                        <option value="Estonian">Estonian</option>
                                        <option value="Farsi">Farsi</option>
                                        <option value="Finnish">Finnish</option>
                                        <option value="Flemish">Flemish</option>
                                        <option value="French">French</option>
                                        <option value="Gallego">Gallego</option>
                                        <option value="Georgian">Georgian</option>
                                        <option value="German">German</option>
                                        <option value="Greek">Greek</option>
                                        <option value="Gujarati">Gujarati</option>
                                        <option value="Haitian Kreyol">Haitian Kreyol</option>
                                        <option value="Hausa">Hausa</option>
                                        <option value="Hebrew">Hebrew</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Hmong">Hmong</option>
                                        <option value="Hungarian">Hungarian</option>
                                        <option value="Icelandic">Icelandic</option>
                                        <option value="Ilokano">Ilokano</option>
                                        <option value="Indonesian">Indonesian</option>
                                        <option value="Italian">Italian</option>
                                        <option value="Japanese">Japanese</option>
                                        <option value="Javanese">Javanese</option>
                                        <option value="Karen">Karen</option>
                                        <option value="Kashmiri">Kashmiri</option>
                                        <option value="Kazakh">Kazakh</option>
                                        <option value="Korean">Korean</option>
                                        <option value="Kurdish">Kurdish</option>
                                        <option value="Ladino">Ladino</option>
                                        <option value="Laotian">Laotian</option>
                                        <option value="Latin">Latin</option>
                                        <option value="Latvian">Latvian</option>
                                        <option value="Lithuanian">Lithuanian</option>
                                        <option value="Maay">Maay</option>
                                        <option value="Macedonian">Macedonian</option>
                                        <option value="Malay">Malay</option>
                                        <option value="Maltese">Maltese</option>
                                        <option value="Marathi">Marathi</option>
                                        <option value="Moldovan">Moldovan</option>
                                        <option value="Mongolian">Mongolian</option>
                                        <option value="Nepali">Nepali</option>
                                        <option value="Norwegian">Norwegian</option>
                                        <option value="Pashto">Pashto</option>
                                        <option value="Polish">Polish</option>
                                        <option value="Portuguese">Portuguese</option>
                                        <option value="Portuguese - Brazilian">Portuguese - Brazilian</option>
                                        <option value="Portuguese - European">Portuguese - European</option>
                                        <option value="Punjabi">Punjabi</option>
                                        <option value="Romanian">Romanian</option>
                                        <option value="Russian">Russian</option>
                                        <option value="Serbian">Serbian</option>
                                        <option value="Sinhalese">Sinhalese</option>
                                        <option value="Slovak">Slovak</option>
                                        <option value="Slovene">Slovene</option>
                                        <option value="Somali">Somali</option>
                                        <option value="Spanish">Spanish</option>
                                        <option value="Swahili">Swahili</option>
                                        <option value="Swedish">Swedish</option>
                                        <option value="Tagalog">Tagalog</option>
                                        <option value="Tamil">Tamil</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Turkish">Turkish</option>
                                        <option value="Twi">Twi</option>
                                        <option value="Ukrainian">Ukrainian</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Uyghur">Uyghur</option>
                                        <option value="Uzbek">Uzbek</option>
                                        <option value="Vietnamese">Vietnamese</option>
                                        <option value="Welsh">Welsh</option>
                                        <option value="Yiddish">Yiddish</option>
                                        <option value="Zulu">Zulu</option>
                                    </select>
                                    <div id="fromLanguage" class="validation"></div>
                                </div>
                                <div class="form-group col-md-5">
                                    <select name="to" id="to" class="form-control" onchange="btn_color(this.value)">
                                        <option value="">Target Language</option>
                                        <option value="English">English</option>
                                        <option value="Afrikaans">Afrikaans</option>
                                        <option value="Albanian">Albanian</option>
                                        <option value="Amharic">Amharic</option>
                                        <option value="Arabic">Arabic</option>
                                        <option value="Armenian">Armenian</option>
                                        <option value="Azeri">Azeri</option>
                                        <option value="Bantu">Bantu</option>
                                        <option value="Belarusan">Belarusan</option>
                                        <option value="Bengali">Bengali</option>
                                        <option value="Bosnian">Bosnian</option>
                                        <option value="Bulgarian">Bulgarian</option>
                                        <option value="Burmese">Burmese</option>
                                        <option value="Cambodian">Cambodian</option>
                                        <option value="Catalan">Catalan</option>
                                        <option value="Chinese">Chinese</option>
                                        <option value="Creole">Creole</option>
                                        <option value="Croatian">Croatian</option>
                                        <option value="Czech">Czech</option>
                                        <option value="Danish">Danish</option>
                                        <option value="Dari">Dari</option>
                                        <option value="Dutch">Dutch</option>
                                        <option value="Estonian">Estonian</option>
                                        <option value="Farsi">Farsi</option>
                                        <option value="Finnish">Finnish</option>
                                        <option value="Flemish">Flemish</option>
                                        <option value="French">French</option>
                                        <option value="Gallego">Gallego</option>
                                        <option value="Georgian">Georgian</option>
                                        <option value="German">German</option>
                                        <option value="Greek">Greek</option>
                                        <option value="Gujarati">Gujarati</option>
                                        <option value="Haitian Kreyol">Haitian Kreyol</option>
                                        <option value="Hausa">Hausa</option>
                                        <option value="Hebrew">Hebrew</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Hmong">Hmong</option>
                                        <option value="Hungarian">Hungarian</option>
                                        <option value="Icelandic">Icelandic</option>
                                        <option value="Ilokano">Ilokano</option>
                                        <option value="Indonesian">Indonesian</option>
                                        <option value="Italian">Italian</option>
                                        <option value="Japanese">Japanese</option>
                                        <option value="Javanese">Javanese</option>
                                        <option value="Karen">Karen</option>
                                        <option value="Kashmiri">Kashmiri</option>
                                        <option value="Kazakh">Kazakh</option>
                                        <option value="Korean">Korean</option>
                                        <option value="Kurdish">Kurdish</option>
                                        <option value="Ladino">Ladino</option>
                                        <option value="Laotian">Laotian</option>
                                        <option value="Latin">Latin</option>
                                        <option value="Latvian">Latvian</option>
                                        <option value="Lithuanian">Lithuanian</option>
                                        <option value="Maay">Maay</option>
                                        <option value="Macedonian">Macedonian</option>
                                        <option value="Malay">Malay</option>
                                        <option value="Maltese">Maltese</option>
                                        <option value="Marathi">Marathi</option>
                                        <option value="Moldovan">Moldovan</option>
                                        <option value="Mongolian">Mongolian</option>
                                        <option value="Nepali">Nepali</option>
                                        <option value="Norwegian">Norwegian</option>
                                        <option value="Pashto">Pashto</option>
                                        <option value="Polish">Polish</option>
                                        <option value="Portuguese">Portuguese</option>
                                        <option value="Portuguese - Brazilian">Portuguese - Brazilian</option>
                                        <option value="Portuguese - European">Portuguese - European</option>
                                        <option value="Punjabi">Punjabi</option>
                                        <option value="Romanian">Romanian</option>
                                        <option value="Russian">Russian</option>
                                        <option value="Serbian">Serbian</option>
                                        <option value="Sinhalese">Sinhalese</option>
                                        <option value="Slovak">Slovak</option>
                                        <option value="Slovene">Slovene</option>
                                        <option value="Somali">Somali</option>
                                        <option value="Spanish">Spanish</option>
                                        <option value="Swahili">Swahili</option>
                                        <option value="Swedish">Swedish</option>
                                        <option value="Tagalog">Tagalog</option>
                                        <option value="Tamil">Tamil</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Turkish">Turkish</option>
                                        <option value="Twi">Twi</option>
                                        <option value="Ukrainian">Ukrainian</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Uyghur">Uyghur</option>
                                        <option value="Uzbek">Uzbek</option>
                                        <option value="Vietnamese">Vietnamese</option>
                                        <option value="Welsh">Welsh</option>
                                        <option value="Yiddish">Yiddish</option>
                                        <option value="Zulu">Zulu</option>
                                    </select>
                                    <div id="toLanguage" class="validation"></div>
                                </div>
                                <div class="form-group col-md-1">
                                    <div class="input-group-btn pull-right">
                                        <button data-toggle="tooltip" id="add_btn" data-placement="top" title="Add More Language!" class="btn" style="background: #cca876;" type="button" class="add-row" onclick="language321();"> Save </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="rTable">
                                    <div class="rTableRow">
                                        <div class="rTableHead"><strong>From Language</strong></div>
                                        <div class="rTableHead"><span style="font-weight: bold;">To Language</span></div>
                                        <div class="rTableHead">Action</div>
                                    </div>
                                    <div id="language_data">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                            </div>
                    </div>
                    <div class="form-row">
                        <div class="clearfix"></div>
                        <br><br>
                        <div class="col-md-12">
                            <hr>
                            <label for="education">Education? </label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="graduate" value="graduate" checked id="graduate" />
                            <label for="graduate">Graduate</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="graduate" value="undergraduate" id="undergraduate" />
                            <label for="undergraduate">Under Graduate</label>
                        </div>
                        <div class="form-group col-md-4" id="fileArea">
                            <label for="fileArea">Attach Diplomas, Certifications, Resume</label>
                            <input type="file" name="file" id="file">
                        </div>
                        <div class="col-md-12">
                            <label for="experience">How many years of translation experience do you have? </label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="experience" checked id="experience1" />
                            <label for="experience1">0-3 years</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="experience" id="experience2" />
                            <label for="experience2">3-6 years</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="experience" id="experience3" />
                            <label for="experience3">More than 6 years</label>
                        </div>
                        <div class="col-md-12">
                            <label for="notarize">Could you provide certified / notarized copy of the translation ?</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="notarize" checked id="notarize1" />
                            <label for="notarize1">Yes</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="notarize" id="notarize2" />
                            <label for="notarize2">No</label>
                        </div>
                        <div class="col-md-12">
                            <label for="services">Please list any additional services you provide : </label>
                            <input type="text" name="extra-service" class="form-control" id="" placeholder="(e.g. transcription, trans creation, voiceover, interpreting, etc. )" data-rule="minlen:1" data-msg="Please Enter atleast one service" />
                            <div class="validation"></div>
                            <br>
                        </div>
                        <div class="col-md-12">
                            <label for="experience">Are you acting as a freelancer or part of an agency? </label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="acting" checked id="radio6" />
                            <label for="radio6">Freelancer</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="acting" id="radio7" />
                            <label for="radio7">Agency</label>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">&nbsp;</label>
                        </div>
                        <div class="col-md-12">
                            <label for="experience">Do you work on a PC or Mac?  </label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="work" checked id="radio4" />
                            <label for="radio4">PC</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="radio" name="work" id="radio5" />
                            <label for="radio5">Mac</label>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="radio5">&nbsp;</label>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" name="per_word" class="form-control" id="per_word" placeholder="Rate per Word" data-rule="minlen:1" data-msg="Please Enter rate per word" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" name="per_page" class="form-control" id="per_page" placeholder="Price per Page" data-rule="minlen:1" data-msg="Please Enter price per page" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" name="per_price" class="form-control" id="per_price" placeholder="Price per Full Day / Half a Day" data-rule="minlen:1" data-msg="Please Enter price per full day / half a day" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-4">
                            <select name="currency" id="currency" class="form-control" data-rule="required" data-msg="Please select a currency">
                                <option value="">Choose Currency</option>
                                <option value="Dollar">Dollar $</option>
                                <option value="euro">Euro &euro;</option>
                            </select>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-8">&nbsp;</div>
                        <div class="form-group col-md-12 tc"> Translation conditions <p>Before proceeding, please accept our Terms & Conditions. We are working to generate a translation environment that will help to produce high quality translations and streamline the overall process. </p>
                        </div>
                        <div class="form-group col-md-12" style="font-size: 14px;">
                            <span>Please indicate that you accept our Terms & Conditions (click for T&Cs in full)</span><br>
                            <input type="checkbox" name="" value="yes" id="accept" onchange="checkTerms();"> <b style="color: rgb(201, 57, 57); margin-top: -30px !important;" id="terms_color"> I accept Terms & Conditions</b><br>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-4" >
                            <input type="password" class="form-control" name="password" id="password" placeholder="Your Password" data-rule="minlen:1" data-msg="Please Enter Password" required
                                   style="display:none" >
                        </div>
                    </div>
                    <div class="text-center"><button type="submit" id="sbtn" disabled class="btn sbtn">Submit</button></div>
                    </form>
                </div> -->
                <div>
                    <form action="" method="post">
                    <div class="one_third first">
            <label for="name">Userame <span>*</span></label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label for="email">Password <span>*</span></label>
            <input type="password" name="password" id="email" value="" size="22" required>
          </div>
         <div class="one_third" style="margin-top:2.8%;">
         <input type="submit" value="Submit">
         </div>
                    </form>
                </div>
                </div>
            </section>
  </main>
</div>

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row4">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_third first">
      <h6 class="heading">Arcu accumsan id felis</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          Street Name &amp; Number, Town, Postcode/Zip
          </address>
        </li>
        <li><i class="fa fa-phone"></i> +00 (123) 456 7890</li>
        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul>
      <ul class="faico clear">
        <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
        <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
        <li><a class="faicon-vk" href="#"><i class="fa fa-vk"></i></a></li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Sodales facilisis nisi</h6>
      <ul class="nospace linklist">
        <li>
          <article>
            <h2 class="nospace font-x1"><a href="#">Curabitur a pharetra</a></h2>
            <time class="font-xs block btmspace-10" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
            <p class="nospace">Quis quam in hendrerit elit donec hendrerit sollicitudin imperdiet curabitur [&hellip;]</p>
          </article>
        </li>
        <li>
          <article>
            <h2 class="nospace font-x1"><a href="#">Risus sit amet blandit</a></h2>
            <time class="font-xs block btmspace-10" datetime="2045-04-05">Thursday, 5<sup>th</sup> April 2045</time>
            <p class="nospace">Ultricies aliquam nisi vitae pulvinar donec luctus ex ex eget ornare tortor [&hellip;]</p>
          </article>
        </li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Justo duis vulputate</h6>
      <p class="nospace btmspace-30">commodo non proin fermentum neque sapien phasellus molestie tincidunt.</p>
      <form method="post" action="#">
        <fieldset>
          <legend>Newsletter:</legend>
          <input class="btmspace-15" type="text" value="" placeholder="Name">
          <input class="btmspace-15" type="text" value="" placeholder="Email">
          <button type="submit" value="submit">Submit</button>
        </fieldset>
      </form>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="#">Domain Name</a></p>
    <p class="fl_right">Template by <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../layout/scripts/jquery.min.js"></script>
<script src="../layout/scripts/jquery.backtotop.js"></script>
<script src="../layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>